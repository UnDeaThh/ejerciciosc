#include <stdio.h> //Librería estandard input output


int main(){

    int sumador1=0;
    int sumador2=0;
    int sumador3=0;
    int resultado;
    
    printf("Bienvenido a la calculadora\n\nDame el primer numero \n");
    scanf("%d",&sumador1);
    getchar();
    
    printf("Dame el siguiente numero: \n");
    scanf("%d",&sumador2);
    getchar();
    
    printf("Necesito otro numero mas: \n");
    scanf("%d",&sumador3);
    getchar();
    
    resultado = 2*sumador1 + 3*sumador2 - 10*sumador3;
    
    printf("El resultado es %d",resultado);
    getchar();
    
    return 0;
}